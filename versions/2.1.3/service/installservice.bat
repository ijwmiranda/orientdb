:: OrientDB Windows Service Installation
@echo off

set JVM_DLL=%JAVA_HOME%\jre\bin\server\jvm.dll
set ORIENTDB_HOME=E:\Software\orientdb-community-2.1.3

set CONFIG_FILE=%ORIENTDB_HOME%/config/orientdb-server-config.xml
SET BUILD_NUMBER=2.1.3
set LOG_FILE=%ORIENTDB_HOME%/config/orientdb-server-log.properties
set LOG_CONSOLE_LEVEL=info
set LOG_FILE_LEVEL=fine
set WWW_PATH=%ORIENTDB_HOME%/www
set ORIENTDB_SETTINGS=-Dstorage.diskCache.bufferSize=7200;-Dprofiler.enabled=true;-Dcache.level2.strategy=1
set JAVA_OPTS_SCRIPT=-XX:+HeapDumpOnOutOfMemoryError;-XX:+PerfDisableSharedMem;-Xmx800m

rem Install service
OrientDBGraph-2.1.3.exe //IS --DisplayName="OrientDB GraphEd DB 2.1.3" --Description="OrientDB Graph Edition 2.1.3, aka GraphEd, contains OrientDB server integrated with the latest release of the TinkerPop Open Source technology stack supporting property graph data model." --StartClass=com.orientechnologies.orient.server.OServerMain --StopClass=com.orientechnologies.orient.server.OServerShutdownMain --Classpath="%ORIENTDB_HOME%\lib\*" --JvmOptions %JAVA_OPTS_SCRIPT%;%ORIENTDB_SETTINGS%;-Djava.util.logging.config.file="%LOG_FILE%";-Dorientdb.config.file="%CONFIG_FILE%";-Dorientdb.www.path="%WWW_PATH%";-Dlog.console.level=%LOG_CONSOLE_LEVEL%;-Dlog.file.level=%LOG_FILE_LEVEL%;-Dorientdb.build.number="%BUILD_NUMBER%";-DORIENTDB_HOME="%ORIENTDB_HOME%" --StartMode=jvm --StartPath="%ORIENTDB_HOME%\bin" --StopMode=jvm --StopPath="%ORIENTDB_HOME%\bin" --Jvm="%JVM_DLL%" --LogPath="%ORIENTDB_HOME%\log" --Startup=auto

pause
EXIT /B
